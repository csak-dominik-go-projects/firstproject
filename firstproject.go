package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("My first GO program!!!")

	bottle := Bottle{
		Level:    0,
		Capacity: 5,
	}

	for i := 0; i < 10; i++ {
		fmt.Println(bottle.String())
		bottle.AddLiquid(1.2)
	}
}

type Bottle struct {
	Level    float64
	Capacity float64
}

func (b *Bottle) AddLiquid(amount float64) {
	var free float64 = b.Capacity - b.Level
	var canAdd float64 = math.Min(free, amount)
	b.Level += canAdd
}

func (b *Bottle) String() string {
	return fmt.Sprintf("%f / %f", b.Level, b.Capacity)
}
